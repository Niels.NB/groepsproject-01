﻿using ExtensionMethods;
using System;
using System.Linq;

namespace Les10_GroepsProject
{
    class Game
    {
        public string Name { get; set; }
        public bool[] Platform { get; set; } // index 0 = PC, 1 = Playstation, 2 = Xbox, 3 = Nintendo
        public int[] InStock { get; set; } // index 0 = PC, 1 = Playstation, 2 = Xbox, 3 = Nintendo
        public decimal SalePrice { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int AgeRating { get; set; } //can ONLY BE 3,7,12,16,18
        public string Genre { get; set; }


        /// <summary>
        /// Constructor for a Game object
        /// </summary>
        /// <param name="initName"></param>
        /// <param name="initConsole">index 0 = PC, 1 = Playstation, 2 = Xbox, 3 = Nintendo</param>
        /// <param name="initInStock">index 0 = PC, 1 = Playstation, 2 = Xbox, 3 = Nintendo</param>
        /// <param name="initSalePrice"></param>
        /// <param name="initReleaseDate">dd/mm/yyyy</param>
        /// <param name="initAgeRating"> Can only be 3, 7, 12, 16, 18</param>
        /// <param name="initGenre"></param>
        public Game(string initName, bool[] initConsole, int[] initInStock, decimal initSalePrice, DateTime initReleaseDate, int initAgeRating, string initGenre)
        {
            Name = initName;
            Platform = initConsole;
            InStock = initInStock;
            SalePrice = initSalePrice;
            ReleaseDate = initReleaseDate;
            SetAgeRating(initAgeRating);
            Genre = initGenre;
        }

        public Game()
        {

        }

        /// <summary>
        /// Used to Set the AgeRating property, if given value is not 3, 7, 12, 16, 18 it will return false
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool SetAgeRating(int value)
        {
            int[] validRating = new int[] { 3, 7, 12, 16, 18 };

            if (validRating.Contains(value))
            {
                AgeRating = value;
                return true;
            }
            else
            {
                System.Console.WriteLine("Geen geldige age rating. Gelieve 3, 7, 12, 16 of 18 in te geven");
                return false;
            }
        }

        public static void PrintHeaders()
        {
            System.Console.Write("{0}{1}", "#".PadRight(6), "Name".PadRight(50));
            MyExtensions.WriteColor("Platform + [In]/", ConsoleColor.DarkGreen);
            MyExtensions.WriteColor("[out] stock".PadRight(29), ConsoleColor.DarkRed);
            System.Console.Write("{0}{1}{2}\n", "Prijs".PadRight(21), "Genre".PadRight(31), "Release date".PadRight(20));
            System.Console.WriteLine("".PadRight(172, '='));
        }

        string GetPlatform()
        {
            string s = "";

            for (int i = 0; i < Platform.Length; i++)
            {
                if (Platform[i])
                {
                    switch (i)
                    {
                        case 0:
                            s += "PC, ";
                            break;
                        case 1:
                            s += "Playstation, ";
                            break;
                        case 2:
                            s += "Xbox, ";
                            break;
                        case 3:
                            s += "Nintendo, ";
                            break;
                    }
                }
            }

            if (!String.IsNullOrEmpty(s))
                s = s.Substring(0, s.Length - 2);

            return s;
        }

        public void PrintPlatformStock()
        {
            string s = "";
            string sLongest = "PC, Playstation, Xbox, Nintendo";
            int lastTrueIndex = Platform.Length;

            for (int i = 0; i < Platform.Length; i++)
            {
                if (Platform[i])
                {
                    if (InStock[i] > 0)
                    {
                        MyExtensions.WriteColor($"[{GetSpecificPlatform(i)}]", ConsoleColor.DarkGreen);
                    }
                    else
                    {
                        MyExtensions.WriteColor($"[{GetSpecificPlatform(i)}]", ConsoleColor.DarkRed);
                    }

                    lastTrueIndex = i;
                    s += GetSpecificPlatform(i);
                }

                if (i < Platform.Length - 1 && (lastTrueIndex < Platform.Length - 1))
                {
                    if (Platform[i + 1])
                    {
                        Console.Write(", ");
                        s += ", ";
                    }
                }

                if (i == Platform.Length - 1)
                {
                    Console.Write("".PadRight(10 + (sLongest.Length - s.Length)));
                }
            }
        }

        public static string GetSpecificPlatform(int index)
        {
            string s = "";
            switch (index)
            {
                case 0:
                    s = "PC";
                    break;
                case 1:
                    s = "Playstation";
                    break;
                case 2:
                    s = "Xbox";
                    break;
                case 3:
                    s = "Nintendo";
                    break;
            }

            return s;
        }

        public string MyToString(int part)
        {
            int padWidth = 50;

            switch (part)
            {
                case 0:
                    return $"{Name.PadRight(padWidth)}";
                case 1:
                    return $"{SalePrice.ToString("C2").PadRight(padWidth - 30)} {Genre.PadRight(padWidth - 20)} {((ReleaseDate.CompareTo(DateTime.Now) == 1) ? ($"[Pre-Order: {ReleaseDate.ToString("dd/MM/yyyy")}]").PadRight(padWidth - 20) : ($"Released:  {ReleaseDate.ToString("dd/MM/yyyy")}").PadRight(padWidth - 20))}";
                case 2: //Return the format needed to write a game away to the GameData.txt
                    string s = "";
                    s += Name;
                    s += ";";

                    for (int i = 0; i < Platform.Length; i++)
                    {
                        if (i < Platform.Length - 1)
                            s += $"{Platform[i]}|";
                        else
                            s += $"{Platform[i]}";
                    }

                    s += ";";

                    for (int i = 0; i < InStock.Length; i++)
                    {
                        if (i < InStock.Length - 1)
                            s += $"{InStock[i]}|";
                        else
                            s += $"{InStock[i]}";
                    }

                    s += ";";

                    s += $"{SalePrice};{ReleaseDate};{AgeRating};{Genre}";

                    return s;
            }

            return new string("empty part in Game.cs");
        }
        public override string ToString()
        {
            int padWidth = 50;

            return $"{Name.PadRight(padWidth)} {GetPlatform().PadRight(padWidth)} {SalePrice.ToString("C2").PadRight(padWidth - 30)} {Genre.PadRight(padWidth - 20)} {((ReleaseDate.CompareTo(DateTime.Now) == 1) ? ($"Pre-Order: {ReleaseDate.ToString("dd/MM/yyyy")}").PadRight(padWidth - 20) : ($"Released:  {ReleaseDate.ToString("dd/MM/yyyy")}").PadRight(padWidth - 20))}";
        }
    }
}
